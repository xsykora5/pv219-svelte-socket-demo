# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/main/packages/create-svelte).

## Running project:

If you're seeing this, you've probably already done this step. Congrats!

```bash
npm install
npm run dev -- --open
```