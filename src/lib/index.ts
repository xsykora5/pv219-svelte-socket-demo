import { writable, type Writable } from 'svelte/store';
import { socket } from '../socket';

export interface MouseCoords {
    x : number;
    y : number;
    name : string;
};

export interface LogoClickEvent {
    name : string
}

export interface ClickCountRow {
    name: string;
    clicks: number;
}
export const CursorStore : Writable<Array<MouseCoords>> = writable([]);


export const cursors: Array<MouseCoords> = [];


 

 
    

