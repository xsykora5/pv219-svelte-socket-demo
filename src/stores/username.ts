import { writable, type Writable } from 'svelte/store';

export const userName : Writable<string> = writable('');