import { io } from "socket.io-client";

// update this to current ip address
const ipAddress = "192.168.1.216"

export const socket = io(`ws://${ipAddress}:4000`);
